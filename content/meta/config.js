module.exports = {
  siteTitle: "Ikonic 2020 - a snowboard tour", // <title>
  shortSiteTitle: "Ikonic 2020 - a snowboard tour", // <title> ending for posts and pages
  siteDescription: "Ikonic 2020 is a snowboard tour blog.",
  siteUrl: "https://ikonic2020.mcquilter.com",
  // pathPrefix: "",
  siteImage: "preview.jpg",
  siteLanguage: "en",

  /* author */
  authorName: "Sheryl Sue McLaughlin Cox and Joshua Edward McLaughlin Cox",
  authorTwitterAccount: "th0th",

  /* info */
  headerTitle: "Josh Cox",
  headerSubTitle: "presents another McQuilter Tour",

  /* manifest.json */
  manifestName: "Ikonic2020 - a McQuilter Tour",
  manifestShortName: "HeroBlog", // max 12 characters
  manifestStartUrl: "/index.html",
  manifestBackgroundColor: "white",
  manifestThemeColor: "#666",
  manifestDisplay: "standalone",

  // gravatar
  // Use your Gravatar image. If empty then will use src/images/jpg/avatar.jpg
  // Replace your email adress with md5-code.
  // Example https://www.gravatar.com/avatar/g.strainovic@gmail.com ->
  // gravatarImgMd5: "https://www.gravatar.com/avatar/1db853e4df386e8f699e4b35505dd8c6",
  gravatarImgMd5: "https://s.gravatar.com/avatar/f4a3774d2a36a5738e79c07700811241",

  // social
  authorSocialLinks: [
    { name: "github", url: "https://github.com/joshuacox" },
    { name: "gitlab", url: "https://gitlab.com/joshuacox" },
    { name: "twitter", url: "https://twitter.com/th0th" },
    { name: "facebook", url: "http://facebook.com/" }
  ]
};
