---
title: author
---

## Ikonic 2020 Galleries

[Main](http://ikonic2020-gallery.mcquilter.com/)

[The Chateau](http://chateau.mcquilter.com/)

[Hotel Arts](http://hotelarts2020.mcquilter.com/)

**Sheryl Sue McLaughlin Cox and Joshua Edward McLaughlin Cox** Two
intrepid travelers seeking fine arts, architecture, and the greatest
earth on show.
