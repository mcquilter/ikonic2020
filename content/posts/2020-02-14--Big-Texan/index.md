---
title: The Big Texan
category: "daily"
cover: front-cow-446.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

#### Breakfast

Texas Omelette

## Departure from BSTX


#### Lunch

K-Bob's Lamesa

Salad bar and CFS

### Amarillo

#### Dinner 

10 beer flight

1. Whiskey Barrel Stout 9.22 ABV  26 IBU
1. Whoop Your Donkey 9.1 ABV 80 IBU
1. Pecan Porter 4.5 ABV 28 IBU
1. Raspberry Wheat 5.1  22 IBU
1. Bomb City Bock 5.5 ABV 13 IBU
1. Texas Red Amber Ale 4.5 ABV 15 IBU
1. Honey Blonde Lager 5.75 ABU 23 IBU
1. El Hefeweizen 5.5 ABV 13 IBU
1. Palo Duro Pale Ale 4.75 ABV 13 IBU
1. Rattlesnake IPA 7.2 ABV 66 IBU

Big Texan Top Sirloin
6 oz Center Cut Sirloin

Big Texan Ribeye
16oz "The Duke's Cut"
