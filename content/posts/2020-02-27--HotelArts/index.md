---
title: Hotel Arts
category: "daily"
cover: face.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

Hotel Arts was a fantastic find.
The art around the entire place was astounding and the
best meal of the trip was at the Yellow Door Bistro!

### Lake Louise, AB

[The Chateau](https://www.fairmont.com/lake-louise/).

#### Breakfast

Poppy Brasserie Breakfast buffet

[breakfast menu](/poppybreakfast.pdf)

### Calgary, AB

[Hotel Arts](https://www.hotelarts.ca/)

#### Dinner 

[Yellow Door Bistro](https://yellowdoorbistro.ca/)

Brussels Sprouts Caesar Salad
prosciutto | sourdough crouton | parmesan | sauce gribiche

Minestrone Ragù Rigatoni
red pepper | zucchini | parmesan | tomato broth

Chocolate Hazelnut Sacher
raspberry crème fraîche | raspberry fluid gel | hazelnut praline | chocolate sauce

3 - course

cherry peanut foie gras

duck breast

whipped strawberry cream on top of a banana crumble


