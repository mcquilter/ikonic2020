---
title: The Trek to Steamboat
category: "daily"
cover: lonerock.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

It was quite a trek to Steamboat, we went southeast from Jackson through
the craziest windstorm we'd ever seen.   Most of I-80 was shutdown
throughout Wyoming.  So we had to leave Rock Springs using 430, which
turned into a dirt road once we hit Colorado!  But we persevered onto
Steamboat.

### Jackson, WY

#### Breakfast

Steak and eggs with toast

#### Steamboat
