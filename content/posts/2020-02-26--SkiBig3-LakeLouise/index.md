---
title: SkiBig3 Lake Louise
category: "daily"
cover: lakelouise.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

### Lake Louise, AB

[The Chateau](https://www.fairmont.com/lake-louise/).

#### Breakfast

Poppy Brasserie Breakfast buffet

[breakfast menu](/poppybreakfast.pdf)

#### Dinner

Lakeview Lounge

[menu](cll-lakeview-allday-menu.pdf)

Duck Wings

Caesar Salad

Game Meat Pappardelle

Steak Frites
