---
title: Steamboat
category: "daily"
cover: steamboat.png
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

Fresh 3" in Steamboat!  And here's the thing, 3" here felt better 7" in
Jackson Hole, better than 20" in Utah!  This was astounding, the entire
mountain had cushy stuff to push around.  The groomed stuff was not as
harsh as the other two and the soft stuff was truly buttery smooth and
nice to push around without so many grindy icy spots poking through.
It may sound like a gimmick, but "champagne powder" truly felt like a
proper name.  Also, the lines were lower, and the mountain was closer to
town (it was practically in the town) than any other place we visited.

#### Steamboat

### Dinner


8th street tater - Very simple but creative desert.
