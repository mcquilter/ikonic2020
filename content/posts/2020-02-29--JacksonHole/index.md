---
title: Jackson Hole
category: "daily"
cover: jacksonhole.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

Wake up in Rexburg a tiny town in Idaho, and on to Jackson Hole!

#### Lunch

### Jackson, WY

[Sidewinders Tavern](https://sidewinderstavern.com/)

Southern Cobb Salad

Hawaiian Burger

[burger menu](/sidewinders-burgers.jpg)

[salad menu](/sidewinders-salad.jpg)

#### Dinner 

Steaks and asparagus
