---
title: Solitude
category: "daily"
cover: solitude.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---
### SLC

#### Breakfast

Cafe in Little America

The Southwestern Omelette - one of my favorites

The Denver Omelette

#### Lunch

[Charlie Chow's Dragon Grill](https://www.charliechowsslc.com/)

This was pretty much like a mongolian BBQ except it was more Chinese in Authenticity, but basically build your own Chinese bowl.  Extremely good, so long as you are good at choosing your ingredients.

#### Dinner 

[Caffe Molise](http://www.caffemolise.com/)

Mediterranea

Imported feta, warmed goat cheese crostini, roasted bell peppers, mixed olives, sundried tomatoes, and marinated roma tomatoes & red onions on fresh greens.  

Bistecca

Beef tenderloin marinated in our secret recipe. Served grilled with a wild mushroom and truffle cream sauce.

Arista

Our staff favorite. Spice-rubbed and oven-roasted pork tenderloin with a black mission fig compote.

