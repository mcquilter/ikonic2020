---
title: Hidden Ridge
category: "daily"
cover: banff.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

### Great Falls, MT

#### Breakfast

Springhill Suites Hotel Buffet

### Banff, AB

#### Dinner 

[Wild Bill's](https://wildbillsbanff.com/)

Wild Bill's Nachos
Cowpuncher Platter
