---
title: The Chateau
category: "daily"
cover: chateau.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

### Banff, AB

#### Breakfast

Bacon Eggs in-house

#### Lunch

sandwiches on the mountain

### Lake Louise, AB

Arrival at [The Chateau](https://www.fairmont.com/lake-louise/).

#### Dinner 

The Fairview


##### 1st 3-course

Lobster with caviar

Bison Ribeye

Simply Chocolate

##### 2nd 3-course

Alberta Farm Salad

Duck Breast

Apple Tatin

[dinner menu](/fairview-dinner.pdf)
[dessert menu](/fairview-dessert.pdf)
