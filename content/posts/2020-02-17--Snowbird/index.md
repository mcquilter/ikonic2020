---
title: Snowbird
category: "daily"
cover: snowbird.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---


#### Breakfast

Cafe in Little America

The Southwestern Omelette - one of my favorites

The Denver Omelette


## Snowbird

President's day!  Do not ever go to the mountain on this day.  Because everyone else will be there too.
First off they got 20 inches so they spent all morning firing a 106 mm howitzer (for real, I overheard a ranger talking about it) at the surrounding mountains to prevent avalanches from hitting the cars.  After this they started letting people drive up the mountain.  Didn't get there till about 10:30, at which point the lifts had not opened!  They were still under avalanche watch.  Around 11 they started up a few of the lifts, those 20 inches were destroyed in about 10 minutes with the crowds chomping at the bit to finally ski that morning.


#### Dinner 

[Himalayan Kitchen](https://www.himalayankitchen.com/)

[full menu here](/himalayan-kitchen-menu.pdf)

Hot Lamb Saag

