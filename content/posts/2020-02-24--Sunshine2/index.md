---
title: Sunshine2
category: "daily"
cover: sunshine2.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

### Banff, AB

#### Breakfast

Bacon Eggs in-house

### Lunch

sandwiches on the mountain

#### Dinner

Pork Shoulder Steaks in-house and potatoes au gratin
