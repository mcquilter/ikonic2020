---
title: Big Sky
category: "daily"
cover: bigsky.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

### Big Sky, MT

#### Breakfast

sandwiches and fruit

### Great Falls, MT

#### Dinner 

[Jakers](https://www.jakers.com/)

[full menu here](/great-falls-dinner-menu.pdf)

Jakers Cap Steam is an amazing steak!

