---
title: Jackson Hole
category: "daily"
cover: jacksonhole.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

Fresh 7" in Jackson Hole! 

### Jackson, WY

#### Breakfast

Steak and eggs with toast

#### Lunch

[Sidewinders Tavern](https://sidewinderstavern.com/)

Southern Cobb Salad

Nachos 

[burger menu](/sidewinders-burgers.jpg)

[salad menu](/sidewinders-salad.jpg)

#### Dinner 

Steak Marsala (poor man's chateau briand)
