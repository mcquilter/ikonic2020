---
title: Sunshine
category: "daily"
cover: banff2.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

### Banff, AB

#### Breakfast

Bacon Eggs in-house

#### Lunch

sandwiches on the mountain

#### Dinner 

Steaks in-house
