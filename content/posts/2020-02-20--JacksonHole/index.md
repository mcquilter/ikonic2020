---
title: Jackson Hole
category: "daily"
cover: jacksonhole.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

### Jackson, WY

#### Breakfast

Bison steak and eggs

Eggs and toast

#### Lunch

[Sidewinders Tavern](https://sidewinderstavern.com/)

Southern Cobb Salad

Ranch Burger

Nachos 

[burger menu](/sidewinders-burgers.jpg)

[salad menu](/sidewinders-salad.jpg)

#### Dinner 

### Big Sky, MT

[Huntley Lodge](https://bigskyresort.com/accommodations/hotels/huntley-lodge)

Beer flight

Mash and Meat flight

Slider Flight

Potatos	

[full menu here](/ChetsDinnerWS1920.pdf)
