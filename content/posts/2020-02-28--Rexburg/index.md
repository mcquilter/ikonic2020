---
title: Rexburg
category: "daily"
cover: rexburg.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---


Breakfast at the Hotel Arts was again amazing like the night before. Then traveled to Rexburg that night on the way to Jackson Hole.

### Calgary, AB

[The Chateau](https://www.fairmont.com/lake-louise/).

#### Breakfast

[Yellow Door Bistro](https://yellowdoorbistro.ca/)
