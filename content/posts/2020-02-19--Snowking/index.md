---
title: Snowking
category: "daily"
cover: snowking.jpg
author: Josh Edward McLaughlin Cox and Sheryl Sue McLaughlin Cox
---

## Departure from SLC


#### Breakfast

Cafe in Little America

The Southwestern Omelette - one of my favorites

The Denver Omelette


#### Dinner 

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d723.8491667625772!2d-110.75508584911319!3d43.47319163387466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x53531a4062b7898f%3A0x58c561a722695517!2sGrand%20View%20Lodge!5e0!3m2!1sen!2sca!4v1582913159278!5m2!1sen!2sca" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

At the Grand View Lodge we had the most creative dish the 'Wild Game Baklava', which was this assorted Elk, Rabbit, Bison and other wild game, cheese, onions, mushrooms  and phylo dough made into this wonderful concoction, that was kind of a meaty baklava.

Bison Tenderloin
