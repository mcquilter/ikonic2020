---
title: Privacy Policy
---

I am not selling any data that you leave behind while visiting this site.  However, there are many third parties between you and this site, and you are on the internet where many such third parties can collect your information.

You should look into using the [dissenter browser](https://dissenter.com/) if you care about free speech and privacy.
